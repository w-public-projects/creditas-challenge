# Creditas Challenge

This is the project for solve the Credita's challenge


[Here is](https://www.linkedin.com/dms/C4E06AQFpRr7KL9GOZw/messaging-attachmentFile/0/1635471451621?m=AQIbi3WCTahzTAAAAXzT5iful9hemTApMIDuXSLdHI0AoIAyyHEbvYEKJw&ne=1&v=beta&t=JBQoo7zjUCpISOjW07K0sOqhDsd12OjMpxAo_5DRIFw) the instructions.



## Installation

For use this project you will need the follow dependencies:

* python 3.8+
* pip 3
* virtualenv
* Docker

You must paste the config.json inside the folder creditas_api/.chalice

## Install dependences and run (the project) in your computer

```bash
cd creditas_api
pip install -r requirements.txt
chalice local --stage=dev
```

## Create and run MongoDb instance with Docker
```bash
sudo docker build -t creditasmongo -f dockerfile_mongo .
sudo sudo docker run -d creditasmongo
```

## Run tests

```bash
cd creditas_api
py.test creditas_api/test/test_unit.py 
```

## deploy to aws

```bash
cd creditas_api
chalice deploy --stage=dev
```

## Endpoints documentation
Click [here](https://documenter.getpostman.com/view/5369879/UVByHq8X) to view documentation.

