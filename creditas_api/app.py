import os
import logging
from chalice import Chalice, Response
from chalicelib.leads_actioner import LeadInsertActioner
from chalicelib.leads_fetcher import LeadFetcher

app = Chalice(app_name='creditas_api')

logger = logging.getLogger(__name__)


@app.route('/', methods=['POST'], api_key_required=True)
def insert_lead():

    lead_received = app.current_request.json_body

    lead_actioner = LeadInsertActioner()

    is_success, result = lead_actioner.save_new_lead(lead_received)

    status_code=201

    if is_success == 0:
        status_code=422

    if is_success == -1:
        status_code=500

    return Response(status_code=status_code, body=result)


@app.route('/{type}', methods=['GET'], api_key_required=True)
def fetch_leads(type):

    leads_obj_fetcher = LeadFetcher()

    res = leads_obj_fetcher.get_results(type)

    if res is None:
        return Response(status_code=204, body=None)

    return res
