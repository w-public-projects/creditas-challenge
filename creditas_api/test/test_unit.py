import json
from chalice.test import Client
from app import app

def test_fetcher():
    with Client(app, stage_name='dev') as client:
        response = client.http.get('/all')
        assert response.status_code == 200

def test_insert():
    with Client(app, stage_name='dev') as client:
        response = client.http.post(
            '/',
            headers={'Content-Type': 'application/json'},
            body=json.dumps({
            "type":3,
            "customer": "Soy una prueba prueba",
            "extra_info": {
                "work_date": "2020-01-01",
                "company_name": "BILLPOCKET"
            }
        })
        )

        assert response.status_code == 201