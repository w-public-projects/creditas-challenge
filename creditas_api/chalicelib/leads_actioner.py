import traceback
import logging
from marshmallow import ValidationError

from .db.leads_db import DBLeads
from .entities.vehicles import Vehicles
from .entities.real_estates import RealEstates
from .entities.payroll import PayRoll
from .helpers.validators import validate_vehicle_lead, validate_building, validate_job_months

logger = logging.getLogger(__name__)


class LeadInsertActioner(DBLeads):

    __validator_types = {}

    def __init__(self):
        super().__init__()

        self.__validator_types = {
            1: {'object': Vehicles(), 'validator': validate_vehicle_lead},
            2: {'object': RealEstates(), 'validator': validate_building},
            3: {'object': PayRoll(), 'validator': validate_job_months}
        }

    def save_new_lead(self, lead_received):

        object_validator = self.__get_object_validation(lead_received)

        if object_validator is False:
            return 0, {"type":["invalid lead"]}

        try:
            parsed_lead = object_validator['object'].load(lead_received)

            parsed_lead['is_valid'] = object_validator['validator'](parsed_lead)

            if parsed_lead['is_valid'] == -1:
                return -1, {"error":["Internal error"]}

        except ValidationError as error:
            return 0, error.messages

        except Exception:
            print(traceback.format_exc())
            return -1, {"error":["Internal error"]}

        self._insert_lead(parsed_lead)

        return 1, lead_received

    def __get_object_validation(self, lead_received):

        if lead_received["type"] in self.__validator_types:
            return self.__validator_types[lead_received["type"]]

        return False




