import os
import requests
import json
from datetime import date
from dateutil import relativedelta


def validate_vehicle_lead(vehicle_data):
    return float(os.environ['min_car_price']) < vehicle_data['general_info']['price'] < float(os.environ['max_car_price'])


def validate_building(building_info):

    zipcode = building_info['general_info']['zipcode']
    url_service = os.environ['zipcode_url_service'].format(token_s=os.environ['zipcode_token_service'], zipcode=zipcode)

    response_service = r = requests.get(url_service)

    response_obj = json.loads(response_service.text)

    if response_service.status_code > 203:
        return -1

    cities_list = json.loads(os.environ['valid_cities'])

    return response_obj['response']['estado'] in cities_list


def validate_job_months(employee_info):

    r = relativedelta.relativedelta(date.today(), employee_info['general_info']['work_date'])

    months_difference = (r.years * 12) + r.months

    # mongodb doesn't support date objects
    employee_info['general_info']['work_date'] = employee_info['general_info']['work_date'].__str__()

    return months_difference > 14

