import os
import traceback
import logging
from pymongo import MongoClient

logger = logging.getLogger(__name__)

class DBLeads:

    __db_connection = None

    def __init__(self):

        self.__init__conn()


    def __init__conn(self):

        client = MongoClient("mongodb://{host}:{port}/".format(host=os.environ['db_host'], port=os.environ['db_port']))

        self.__db_connection = client[os.environ['db_schema']]

    def _insert_lead(self, formated_lead):

        collection = self.__db_connection['leads']

        try:
            return collection.insert_one(formated_lead)

        except Exception:
            print(traceback.format_exc())
            return False

    def _fetch_leads(self, criteria):

        collection = self.__db_connection['leads']

        if criteria == 'all':
            return collection.find()

        if criteria.isnumeric() is False:
            return None

        return collection.find({'type': int(criteria)})