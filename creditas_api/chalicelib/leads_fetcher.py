from .db.leads_db import DBLeads
from .responses.payroll import PayRoll
from .responses.real_estates import RealEstates
from .responses.vehicles import Vehicles


class LeadFetcher(DBLeads):

    __transformers=None

    def __init__(self):
        super().__init__()

        self.__transformers = {
            1: {'object': Vehicles()},
            2: {'object': RealEstates()},
            3: {'object': PayRoll()}
        }

    def get_results(self, criteria):

        results = self._fetch_leads(criteria)

        if results is None:
            return None

        transformed_results = []

        for r in results:
            obj_transform = self.__transformers[r['lead_type']]['object']
            transformed_results.append(obj_transform.dump(r))

        return transformed_results