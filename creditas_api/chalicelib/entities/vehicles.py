from marshmallow import Schema, fields
from .parent_entity import Leads


class VehicleInfo(Schema):
    year = fields.Integer(data_key='year', required=True)
    car_brand = fields.String(data_key='brand', required=True)
    model_name = fields.String(data_key='model', required=True)
    price = fields.Float(data_key='cost', required=True)


class Vehicles(Leads):
    general_info = fields.Nested(VehicleInfo(), required=True, data_key='extra_info')


