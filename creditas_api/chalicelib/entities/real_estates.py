from datetime import date
from marshmallow import Schema, fields
from .parent_entity import Leads


class RealEstateInfo(Schema):
    year = fields.Integer(data_key='year') # building year
    zipcode = fields.String(data_key='zipcode')
    street = fields.String(data_key='street')
    city = fields.String(data_key='city')
    state = fields.String(data_key='state')
    country = fields.String(dump_default='MX')


class RealEstates(Leads):
    general_info = fields.Nested(RealEstateInfo(), required=True, data_key='extra_info')