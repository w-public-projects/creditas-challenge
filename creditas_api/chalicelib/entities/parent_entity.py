from datetime import datetime
from marshmallow import Schema, fields


class Leads(Schema):
    lead_type = fields.Integer(data_key='type') # 1 vehicles, 2 houses, 3 payroll
    created_at = fields.DateTime(load_default=datetime.utcnow())
    update_at= fields.DateTime(load_default=datetime.utcnow())
    client_name = fields.String(data_key='customer')
    is_valid = fields.Boolean(load_default=True)