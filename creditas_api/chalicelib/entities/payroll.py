from datetime import date
from marshmallow import Schema, fields
from .parent_entity import Leads


class PayRollInfo(Schema):
    work_date = fields.Date(required=True)
    company_name = fields.String(required=True)


class PayRoll(Leads):
    general_info = fields.Nested(PayRollInfo(), required=True, data_key='extra_info')