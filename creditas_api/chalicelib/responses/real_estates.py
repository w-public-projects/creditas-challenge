from datetime import date
from marshmallow import Schema, fields
from .parent_entity_response import Leads


class RealEstateInfo(Schema):
    year = fields.Integer(attribute='year', required=True) # building year
    zipcode = fields.String(attribute='zipcode', required=True)
    street = fields.String(attribute='street', required=True)
    city = fields.String(attribute='city', required=True)
    state = fields.String(attribute='state', required=True)
    country = fields.String(dump_default='MX', required=True)


class RealEstates(Leads):
    extra_info = fields.Nested(RealEstateInfo(), required=True, attribute='general_info')