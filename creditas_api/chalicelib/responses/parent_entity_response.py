from datetime import datetime
from marshmallow import Schema, fields


class Leads(Schema):
    type = fields.Integer(attribute='lead_type') # 1 vehicles, 2 houses, 3 payroll
    created_at = fields.DateTime()
    update_at= fields.DateTime()
    customer = fields.String(attribute='client_name')
    is_valid = fields.Boolean()