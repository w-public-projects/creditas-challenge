from marshmallow import Schema, fields
from .parent_entity_response import Leads


class VehicleInfo(Schema):
    year = fields.Integer(attribute='year', required=True)
    brand = fields.String(attribute='car_brand', required=True)
    model_name = fields.String(attribute='model', required=True)
    cost = fields.Float(attribute='price', required=True)


class Vehicles(Leads):
    extra_info = fields.Nested(VehicleInfo(), required=True, attribute='general_info')


