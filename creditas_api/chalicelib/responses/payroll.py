from datetime import date
from marshmallow import Schema, fields
from .parent_entity_response import Leads


class PayRollInfo(Schema):
    work_date = fields.String(required=True)
    company_name = fields.String(required=True)


class PayRoll(Leads):
    extra_info = fields.Nested(PayRollInfo(), required=True, attribute='general_info')